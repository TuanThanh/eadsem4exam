package com.example.exam.webapp;

import com.example.exam.domain.Employee;
import com.example.exam.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class EmployeeController {
    @Autowired
    private EmployeeService employeeService;

    @RequestMapping("/")
    public String index(Model model){
        List<Employee> employees = employeeService.getAllEmployees();

        model.addAttribute("employees", employees);

        return "index";
    }

    @RequestMapping(value = "add")
    public String addUser(Model model){
        model.addAttribute("employee", new Employee());
        return "addEmployee";
    }
}
